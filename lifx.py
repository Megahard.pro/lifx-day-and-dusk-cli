import lifxlan as lx
import time
import math
import argparse

def perc(perc): 
	return math.floor((65535 / 100) * perc)

# create parser
parser = argparse.ArgumentParser()
 
# add arguments to the parser
parser.add_argument("-c", "--colortemp", nargs='?', default = 1500, choices=range(1500, 7501), metavar = " 1500 to 7500 K", type=int, help="color temperature of bulb")
parser.add_argument("-b", "--brightness", nargs='?', default = 100, choices=range(0, 101),		metavar = " 0 to 100 %",type=int, help="brightness of bulb")
parser.add_argument("-d", "--duration", nargs='?', default = 1000, choices=range(0, 20000),		metavar = " 0 to 20000 mS",type=int, help="transition duration")

# parse the arguments
args = parser.parse_args()
 
# get the arguments value
ltemp = args.colortemp
lbright = args.brightness
ldur = args.duration
# Set LIFX color list
lcolor = [0,0,perc(lbright), ltemp]

lan = lx.LifxLAN(2)   # find max 2 lights

lights = lan.get_lights()

for light in lights: 
	print("Lights found: " + light.get_label() + "\t" + str(light.get_color()[3]))
	light.set_color(lcolor, ldur)
	print("Lights set: " + light.get_label() + "\t" + str(light.get_color()[3]))


