This tiny script is meant to control mini day & dusk bulbs only. 

usage: lifx.py [-h] [-c [1500 to 7500 K]] [-b [0 to 100 %]] [-d [0 to 20000 mS]]

optional arguments:
  -h, --help            show this help message and exit
  -c [ 1500 to 7500 K], --colortemp [ 1500 to 7500 K]
                        color temperature of bulb
  -b [ 0 to 100 %], --brightness [ 0 to 100 %]
                        brightness of bulb
  -d [ 0 to 20000 mS], --duration [ 0 to 20000 mS]
                        transition duration